


$(
  function(){
    $('[name="inline-ajax-login-credential"]').click(function(){ialCheckCredential();});

    ialCheckCredential();
  }
);



function ialCheckCredential(){

  if ($('#edit-inline-ajax-login-credential-name').attr('checked')){
  
    $('#edit-inline-ajax-login-register-name').attr('checked', 'checked').attr('disabled', 'disabled');

    if ($('#edit-inline-ajax-login-request-mail').attr('checked')){
      $('#edit-inline-ajax-login-request-interval').attr('checked', true);
    }
    $('#edit-inline-ajax-login-request-mail').attr('disabled', 'disabled');

  } else {

    $('#edit-inline-ajax-login-request-mail').removeAttr('disabled');
    $('#edit-inline-ajax-login-register-name').removeAttr('disabled', 'disabled');

  }
}